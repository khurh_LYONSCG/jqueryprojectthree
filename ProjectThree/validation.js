$(document).ready(function(){
   
    $('a').targetBlank();
    $('input[type=text]').highlightInput();

    $('#billing-page').validate({
        rules: {
            firstname: {
                required: true,
                lettersonly: true        
            },
            lastname: {
                required: true,
                lettersonly: true        
            },
            email: {
                required: true,
                email: true
            },     
            phonenumber: {
                required: true,
                phoneUS: true
            },
            cardnumber: {
                required: true,
                creditcard: true
            },
            expmonth: {
                required: true,
                digits: true
            },
            expyear: {
                required: true,
                digits: true
            },
            zipcode: {
                required: true,
                digits: true,
                zipcodeUS: true
            },
            cvc: {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 3
            }
        },
        messages: {
            email: {
                email: 'Please enter a valid email address.'
            },
            expmonth: {
                digits: 'Select a month.'
            },
            expyear: {
                digits: 'Select a year.'
            },
            zipcode: {
                zipcodeUS: 'Enter a US ZIP Code.'
            },
            cvc: {
                minlength: 'Must contain 3 digits.',
                maxlength: 'Must contain 3 digits.'
            }

        }
    });

    $(function(){
        $("#phonenumber").mask("999-999-9999");
        $("#phonenumber").on("blur", function() {
            var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
            if( last.length == 5 ) {
                var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );
                var lastfour = last.substr(1,4);
                var first = $(this).val().substr( 0, 9 );
                $(this).val( first + move + '-' + lastfour );
            }
        });
      });

});
